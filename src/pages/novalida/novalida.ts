import { Component } from '@angular/core';
import { NavController, NavParams , LoadingController} from 'ionic-angular';
import { AutenticacuentaProvider } from '../../providers/autenticacuenta/autenticacuenta';
import { Cuenta } from '../../app/cuenta.entity';
import { MicuentaPage } from '../micuenta/micuenta';
import { HomePage } from '../home/home';

@Component({
  selector: 'page-novalida',
  templateUrl: 'novalida.html',
})
export class NovalidaPage {

  mensajeError:string;
  uuid:string;
  cuenta:Cuenta;
  
  MSG_CANCEL = "La cuenta continua cancelada, verifica con el administrador el estado de tu cuenta.";


  constructor(public navCtrl: NavController, public navParams: NavParams,
  private autenticaProvider: AutenticacuentaProvider, private loadingCtrl: LoadingController) {
     console.log('novalida');
    
  }

  ionViewDidLoad() {
    this.mensajeError = this.navParams.get('mensaje');
    this.uuid = this.navParams.get('uuid');

    console.log(this.uuid);
   
  }

  validaDispositivo(){
    let loader = this.loadingCtrl.create({
      content: "Validando"
    });
    loader.present();
    this.autenticaProvider.getMiembroByUUID(this.uuid).subscribe(result => {
      console.log(result);
      //Validamos si el resultado tiene un objeto error
      if (result.error) {
        loader.dismiss();
        this.mensajeError = result.error;
      } else {
        //Pasamos el objeto a Cuenta.
        this.cuenta = new Cuenta();
        this.cuenta.id = result[0].idt_miembros;
        this.cuenta.nombreCompleto = result[0].nombre_completo;
        this.cuenta.email = result[0].email;
        this.cuenta.estado = result[0].estado;
        this.cuenta.fechaIngreso = new Date();
        this.cuenta.fechaRegistro = result[0].fecha_registro;
        this.cuenta.isActivo = result[0].is_activo;
        this.cuenta.telefonoPrincipal = result[0].telefono_principal;

        if(this.cuenta.estado == 0 ){
          loader.dismiss();
          this.mensajeError = this.MSG_CANCEL;
        } else {
          if(this.cuenta.isActivo == 1){
              loader.dismiss();
            this.navCtrl.setRoot(MicuentaPage,{uuid:this.uuid});

          } else {
            loader.dismiss();
            this.navCtrl.setRoot(HomePage);
          }
        }

      }
    }, err => {
      loader.dismiss();
      console.log("ERROR => " + err);
    });
  }

  presentLoading() {
    let loader = this.loadingCtrl.create({
      content: "Validando..",
      duration: 1000
    });
    loader.present();
  }
}
