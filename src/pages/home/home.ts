import { Component, OnInit} from '@angular/core';
import { NavController, LoadingController, ToastController, AlertController } from 'ionic-angular';
import { Device } from '@ionic-native/device';
import { NovalidaPage } from '../novalida/novalida';
import { AutenticacuentaProvider } from '../../providers/autenticacuenta/autenticacuenta';
import { Cuenta } from '../../app/cuenta.entity';
import { MicuentaPage } from '../micuenta/micuenta';

import { Storage } from '@ionic/storage';
import { RegistroPage } from '../registro/registro';
import { AutenticacionProvider } from '../../providers/autenticacion/autenticacion';
import { LandingPage } from '../landing/landing';

import { Events } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit{

  KEY_CODE_DEVICE = "codeDevice";

  validCode = true;

  uuid: string;
  cuenta: Cuenta;

  email: string;
  password: string;

  MSG_CANCEL = "La cuenta asociada a este dispositivo esta cancelada, verifica con el administrador el estado de tu cuenta.";

  constructor(public navCtrl: NavController, private device: Device,
    private autenticaProvider: AutenticacuentaProvider, private loadingCtrl: LoadingController,
    private toastCtrl: ToastController, private storage: Storage,
    private autenticacionProvider: AutenticacionProvider, public alertCtrl: AlertController,
    public events: Events) {
  }

  ngOnInit() {
    
  }
    

  goRegistro() {
    this.navCtrl.push(RegistroPage);
  }

  autentica() {
    console.log(this.email, this.password);
    if (this.email == undefined || this.password == undefined) {
      let alert = this.alertCtrl.create({
            title: "Ups...",
            message: "Te falta poner el usuario o la contraseña",
            buttons: ['OK']
          });
          alert.present();
    } else {
      this.autenticacionProvider.iniciarSesion(this.email, this.password)
        .then(info => {
          console.log(info);
          this.storage.set('isLoggedIn',true);
          this.storage.set('userlogged',this.email);
          this.storage.set('passwordlogin',this.password);

          this.events.publish('user:login', this.email, Date.now());

          this.autenticacionProvider.terminaSesion();
          this.navCtrl.setRoot(LandingPage);
        })
        .catch(error => {
          let alert = this.alertCtrl.create({
            title: "Ups..",
            message: error.message,
            buttons: ['OK']
          });
          alert.present();
        });
    }
  }





  getCuenta() {
    this.autenticaProvider.getMiembroByUUID(this.uuid).subscribe(result => {
      console.log(result);
      //Validamos si el resultado tiene un objeto error
      if (result.error) {
        console.log(this.uuid);
        let toast = this.toastCtrl.create({
          message: result.error,
          duration: 2000,
          position: 'middle'
        });
        toast.present();
        return false;
      } else {
        //Pasamos el objeto a Cuenta.
        this.cuenta = new Cuenta();
        this.cuenta.id = result[0].idt_miembros;
        this.cuenta.nombreCompleto = result[0].nombre_completo;
        this.cuenta.email = result[0].email;
        this.cuenta.estado = result[0].estado;
        this.cuenta.fechaIngreso = result[0].fecha_ingreso;
        this.cuenta.fechaRegistro = result[0].fecha_registro;
        this.cuenta.isActivo = result[0].is_activo;
        this.cuenta.telefonoPrincipal = result[0].telefono_principal;

        if (this.cuenta.estado == 0) {
          console.log(this.uuid);
          this.navCtrl.setRoot(NovalidaPage, { mensaje: this.MSG_CANCEL, uuid: this.uuid });
        } else {
          if (this.cuenta.isActivo == 1) {
            console.log(this.uuid);
            this.navCtrl.setRoot(MicuentaPage, { uuid: this.uuid });
          } else {
            return true;
          }
        }

      }
    }, err => {
      let toast = this.toastCtrl.create({
        message: 'Falló al validar la cuenta',
        duration: 2000,
        position: 'middle'
      });
      toast.present();
      return false;
    });
  }

  activaCuenta() {
    console.log("ActivaCuenta");
    let loader = this.loadingCtrl.create({
      content: "Activando, espera unos segundos...",
      dismissOnPageChange: true,
      duration: 1000
    });
    loader.present();

    if (this.cuenta) {
      console.log("Existe cuenta");


      this.autenticaProvider.activaCuenta(this.cuenta.id).subscribe(result => {
        if (result.success) {
          this.navCtrl.setRoot(MicuentaPage, { uuid: this.uuid });
        }
      }, err => {
        let toast = this.toastCtrl.create({
          message: 'Falló al activar la cuenta',
          duration: 1500,
          position: 'middle'
        });
        toast.present();
      });
    } else {
      let toast = this.toastCtrl.create({
        message: 'Falló al validar la cuenta',
        duration: 1500,
        position: 'middle'
      });
      toast.present();

    }
  }

  obtieneYactivaCuenta() {
    console.log("obtieneYactivaCuenta inicia");
    this.getCuenta();
    this.activaCuenta();
  }

  /**
   * Metodo para generar el codigo del dispositivo.
   */
  makeid() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 8; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    this.storage.set(this.KEY_CODE_DEVICE, text.toUpperCase());
    this.uuid = text.toUpperCase();
  }


}
