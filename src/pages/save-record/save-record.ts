import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { DetalleCategoria } from '../../app/wod.entity';
import { WodsProvider } from '../../providers/wods/wods';
import { Utils } from '../../app/utils';

@Component({
  selector: 'page-save-record',
  templateUrl: 'save-record.html',
})
export class SaveRecordPage {

  marca: DetalleCategoria;
  listadoWods;
  idMiembro:number;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public viewCtrl: ViewController, private alertCtrl: AlertController,
    private wodProvider:WodsProvider) {
    this.marca = new DetalleCategoria();
    this.listadoWods = navParams.get('wods');
    this.idMiembro = navParams.get('miembro');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SaveRecordPage');
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }

  saveMarca() {
    console.log(this.marca);
    const totaltimecap = this.marca.minuteTotal + ':' + this.marca.secondTotal;
    if (this.validMarca(this.marca.repsTotal, this.marca.minuteTotal, this.marca.secondTotal)) {
      this.wodProvider.saveResultadoWODCategoria(
        this.marca.wod.id, this.idMiembro, this.marca.idCategoria, this.marca.repsTotal, Utils.getSecodsFromTime(totaltimecap),this.marca.peso)
        .subscribe(result => {
          console.log(result);
          if (result.error) {
            this.showAlert('Ups!!', 'No se pudo registrar tu marca intente mas tarde.');
          } else {
            this.showAlert('Exito!!', 'Se registro tu marca para este WOD.');
            this.closeModal();
          }
        });
    } else {
      this.showAlert('Cuidado!!', 'Revisa tu marca puede que no este completa.');
    }
  }

  validMarca(reps, minute, seconds) {
    let isValid = true;
    if (!reps || reps === '') {
      isValid = false;
    }
    if (!minute || minute === '') {
      isValid = false;
    }
    if (!seconds || seconds === '') {
      isValid = false;
    }

    return isValid;
  }

  showAlert(title: string, subtitle: string) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: subtitle,
      buttons: ['OK']
    });
    alert.present();
  }
}
