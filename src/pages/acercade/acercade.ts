import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AutenticacionProvider } from '../../providers/autenticacion/autenticacion';
import { HomePage } from '../home/home';

@Component({
  selector: 'page-acercade',
  templateUrl: 'acercade.html',
})
export class AcercadePage {

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private autenticacionProvider: AutenticacionProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AcercadePage');
  }

  terminaSesion() {
    this.autenticacionProvider.terminaSesion()
      .then(info => {
        this.navCtrl.setRoot(HomePage);
      });
  }
}
