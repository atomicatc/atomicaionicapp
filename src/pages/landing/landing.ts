import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';

import { HomePage } from '../home/home';
import { AutenticacionProvider } from '../../providers/autenticacion/autenticacion';
import { DetailWodPage } from '../detail-wod/detail-wod';
import { WodsProvider } from '../../providers/wods/wods';
import { DetalleCategoria } from '../../app/wod.entity';

import { Events } from 'ionic-angular';
import { MismarcasPage } from '../mismarcas/mismarcas';
import { RankingPage } from '../ranking/ranking';

@Component({
  selector: 'page-landing',
  templateUrl: 'landing.html',
})
export class LandingPage implements OnInit {

  user: string;
  password: string;
  todayWod: Date;
  bestRecord: DetalleCategoria;
  accessCode: string;
  idMiembro: number = 0;
  fechaSiguientePago:Date;

  messagePay:string;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private storage: Storage, private autenticaProvider: AutenticacionProvider,
    private barcode: BarcodeScanner, private wodsProvider: WodsProvider,
    private alertCtrl: AlertController, private events: Events) {
    this.todayWod = new Date();
    this.bestRecord = new DetalleCategoria();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LandingPage');
  }

  ngOnInit() {
    this.storage.get('userlogged').then(user => {
      this.user = user;
      if (user) {
        this.storage.get('passwordlogin').then(passwd => {
          this.autenticaProvider.iniciarSesion(user, passwd).then(console.log);
          console.log('logedin');
          this.autenticaProvider.getMiembroByEmail(user).subscribe(result => {
            console.log(result[0]);
            this.idMiembro = result[0].idt_miembros;
            this.storage.set('userName', result[0].nombre_completo);

            this.events.publish('user:login', result[0].nombre_completo, true, '', this.idMiembro);
            //this.loadTodayWOD();
            /**
             * Validamos la imagen almacenada dentro del dispositivo.
             */
            this.storage.get('imgProfile').then(val => {
              if (val) {
                this.events.publish('user:updatepic', val);
              }
            });

            /**
             * Asignamos el nuevo codigo que se obtuvo.
             */
            this.accessCode = result[0].uuid;
            /** Validamos la fecha para presentar el mensaje especifico */
            this.fechaSiguientePago = result[0].fecha_siguiente_pago;
            /** Calculamos el rango desde el dia actual + 7 dias y validamos contra la fecha del siguiente pago
             * Si esta antes de la fecha inicio del periodo se presenta mensaje de falta de pago.
             * Si esta entre las fecha del rango presenta mensaje de advertencia indicando su proxima fecha de pago.
             * Si esta dspues de la fecha no presenta mensaje.
             */
            
          });
        });
      } else {
        this.navCtrl.setRoot(HomePage);
      }
    });

  }

 

  //Generamos metodo para codificar el QR y unirlo al boton.
  async encodeData() {
    this.barcode.encode(this.barcode.Encode.TEXT_TYPE, this.accessCode);
  }

  /** 
   * Obtenemos el datalle del Wod, Ejercicios y categorias.
   */
  goDetailWOD() {
   /* if (this.todayWod.id === 0) {
      this.showAlert('Lo siento!', 'El wod del dia aun no ha sido asignado.');
    } else {
      this.navCtrl.push(DetailWodPage, { wod: this.todayWod, idMiembro: this.idMiembro, isEditable: true });
    }*/
    this.navCtrl.push(DetailWodPage, { idMiembro: this.idMiembro, isEditable: true, dayWod: new Date() });
  }

  goMarcas() {
    this.navCtrl.setRoot(MismarcasPage, { idMiembro: this.idMiembro });
  }

  goRanking() {
    this.navCtrl.push(RankingPage, { wod: this.todayWod });
  }

  showAlert(title: string, subtitle: string) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: subtitle,
      buttons: ['OK']
    });
    alert.present();
  }

  /**
   * Evento del regresher para actualizacion de datos.
   * @param refresher Objeto de vista.
   */
  doRefresh(refresher) {
   /* this.loadTodayWOD();*/
    refresher.complete();
  }

  /**
   * Metodo para indicar que esta en construccion.
   */
  underConstruction() {
    this.showAlert('Pronto!!', 'En construcción.');
  }
}
