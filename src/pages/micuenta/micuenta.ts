import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController, Platform, LoadingController, Loading } from 'ionic-angular';
import { Cuenta } from '../../app/cuenta.entity';
import { AutenticacuentaProvider } from '../../providers/autenticacuenta/autenticacuenta';
import { PagosProvider } from '../../providers/pagos/pagos';
import { Pago } from '../../app/pagos.entity';

import { File } from '@ionic-native/file';
import { Transfer } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';

import { Storage } from '@ionic/storage';
import { AcercadePage } from '../acercade/acercade';
import { AutenticacionProvider } from '../../providers/autenticacion/autenticacion';

import { Events } from 'ionic-angular';
import { Utils } from '../../app/utils';

declare var cordova: any;

@Component({
  selector: 'page-micuenta',
  templateUrl: 'micuenta.html',
})
export class MicuentaPage implements OnInit {

  KEY_PATH_IMG_PROFILE = "imgProfile";
  KEY_CODE_DEVICE = "codeDevice";

  lastImage: string = null;
  fullPathImage: string = './assets/img/profile.png';
  loading: Loading;

  messageEmpty: string;

  email: string;
  cuenta = new Cuenta();
  listaPagos = new Array();

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private autenticaProvider: AutenticacuentaProvider, private pagosProvider: PagosProvider,
    private camera: Camera, private transfer: Transfer, private file: File, private filePath: FilePath,
    public toastCtrl: ToastController, public platform: Platform, public loadingCtrl: LoadingController,
    private storage: Storage, private autenticacionProvider: AutenticacionProvider, private events: Events) {
    storage.get('userlogged').then(email => {
      this.email = email;
      this.cargaCuenta(email);
    });

    storage.get(this.KEY_PATH_IMG_PROFILE).then((val) => {
      if (val) {
        this.fullPathImage = val;
      }
    });
  }

  ngOnInit() {

  }

  ionViewDidLoad() {


  }

  showAcercade() {
    this.navCtrl.push(AcercadePage);
  }

  cargaCuenta(email) {
    console.log('email: ' + email);
    this.autenticacionProvider.getMiembroByEmail(email).subscribe(result => {
      //Pasamos el objeto a Cuenta.

      this.cuenta.id = result[0].idt_miembros;
      this.cuenta.nombreCompleto = result[0].nombre_completo;
      this.cuenta.email = result[0].email;
      this.cuenta.estado = result[0].estado;
      this.cuenta.fechaIngreso = new Date(result[0].fecha_ingreso * 1);
      this.cuenta.fechaRegistro = result[0].fecha_registro;
      this.cuenta.fechaRegistroCadena = Utils.formatDate(result[0].fecha_registro);
      this.cuenta.isActivo = result[0].is_activo;
      this.cuenta.telefonoPrincipal = result[0].telefono_principal;
      this.cuenta.uuid = result[0].uuid;
      this.cuenta.fechaUltimoPago = result[0].fecha_ultimo_pago;
      this.cuenta.fechaSiguientePago = result[0].fecha_siguiente_pago;
      this.cuenta.objetivo = result[0].objetivo;
      this.cuenta.consideraciones = result[0].consideraciones;

      this.listaPagos = new Array();
      this.pagosProvider.getPagosActivosByMiembroOrdeByNewest(this.cuenta.id).subscribe(result => {
        console.log(result);
        if (result.error) {
          this.messageEmpty = result.error;
        } else {
          for (let entry of result) {
            console.log(entry);
            let pago = new Pago();

            pago.id = entry.idt_historial_pagos;
            pago.concepto = entry.concepto;
            pago.importe = entry.importe;
            pago.fechaRegistro = entry.fecha_registro;
            pago.fechaRegistroCadena = Utils.formatDate(entry.fecha_registro);

            this.listaPagos.push(pago);
          }
        }
      }, err => {
        console.log("ERROR => " + err);
      });
    }, err => {
      console.log("ERROR => " + err);
    });
  }

  /**
   * Metodo para cargar un archivo.
   */
  loadPicture() {
    this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
  }

  /**
   * Metodo para tomar una foto.
   */
  useCamera() {
    this.takePicture(this.camera.PictureSourceType.CAMERA);
  }

  /**
   * Metodo para eliminar la foto.
   */
  dropPicture() {
    this.fullPathImage = "./assets/img/profile.png";
    this.storage.set(this.KEY_PATH_IMG_PROFILE, null);
  }

  public takePicture(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      quality: 100,
      sourceType: sourceType,
      saveToPhotoAlbum: true,
      correctOrientation: true,
      targetWidth: 450,
      targetHeight: 450,
      allowEdit: true,
      cameraDirection: 1
    };

    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      // Special handling for Android library
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath)
          .then(filePath => {
            let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
          });
      } else {
        var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
      }
    }, (err) => {
      this.presentToast('No se logro utilizar la foto.');
    });
  }

  // Create a new name for the image
  private createFileName() {
    var d = new Date(),
      n = d.getTime(),
      newFileName = n + ".jpg";
    return newFileName;
  }

  // Copy the image to a local folder
  private copyFileToLocalDir(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
      this.lastImage = newFileName;
      this.fullPathImage = this.pathForImage(this.lastImage);
      this.storage.set(this.KEY_PATH_IMG_PROFILE, this.fullPathImage);
      /**Cuando se actualice la foto tambien se acutalizara la foto del menu */
      this.events.publish('user:updatepic', this.fullPathImage);
    }, error => {
      this.presentToast('Error al guardar la foto.');
    });
  }

  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'middle'
    });
    toast.present();
  }

  // Always get the accurate path to your apps folder
  public pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      return cordova.file.dataDirectory + img;
    }
  }

  doRefresh(refresher) {

    this.cargaCuenta(this.email);
    refresher.complete();
  }

  updateAccount() {
    this.autenticacionProvider.updateDateAccount(this.cuenta.id, this.cuenta.nombreCompleto, this.cuenta.objetivo,
      this.cuenta.consideraciones, this.cuenta.telefonoPrincipal).subscribe(resultUpdate => {
        if (resultUpdate.error) {
          this.presentToast(resultUpdate.error);
        } else {
          this.presentToast(resultUpdate.success);
        }
      });
  }

}
