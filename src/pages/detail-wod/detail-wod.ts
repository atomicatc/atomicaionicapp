import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, AlertController } from 'ionic-angular';
import { WOD, WodEjercicio, DetalleCategoria } from '../../app/wod.entity';
import { WodsProvider } from '../../providers/wods/wods';
import { SaveRecordPage } from '../save-record/save-record';
import { CalendarModalOptions, CalendarComponentOptions, CalendarModal } from 'ion2-calendar';

@Component({
  selector: 'page-detail-wod',
  templateUrl: 'detail-wod.html',
})
export class DetailWodPage {

  idMiembro: number = 0;
  wod: WOD;
  ejercicios: Array<WodEjercicio>;
  ejerCategorias: Array<DetalleCategoria>;

  todayWod: Date;
  listadoWods: Array<WOD>;
  emptyWods: string;

  date: string;
  type: 'string';

  showCalendar: Boolean = false;

  options: CalendarComponentOptions = {
    from: new Date(2017, 0, 1),
    to: new Date(2018, 11, 31),
    pickMode: 'single'
  };

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private wodProvider: WodsProvider, private modalCtrl: ModalController,
    private alertCrtl: AlertController) {
    this.todayWod = this.navParams.get('dayWod');

    //this.wod = this.navParams.get('wod');
    this.idMiembro = this.navParams.get('idMiembro');
    this.listadoWods = new Array<WOD>();
    this.ejercicios = new Array<WodEjercicio>();
    this.ejerCategorias = new Array();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailWodPage');
    this.loadWodDetails();
  }

  loadWodDetails() {
    this.listadoWods = new Array<WOD>();
    this.wodProvider.getDayWods(this.todayWod).subscribe(resultWods => {
      if (resultWods.error) {
        this.emptyWods = resultWods.error;
      } else {
        for (const resWOD of resultWods) {
          const wod = new WOD();

          wod.id = resWOD.idc_wods;
          wod.nombre = resWOD.nombre;
          wod.tipo = resWOD.tipo;
          wod.tipoDesc = resWOD.tipo_wod;
          wod.timecap = resWOD.timecap;
          wod.participacion = resWOD.participacion;
          wod.detalle = resWOD.detalle;
          wod.ejercicios = new Array();

          this.wodProvider.getEjerciciosByWod(wod.id).subscribe(resultEjercicios => {
            console.log(resultEjercicios);
            if (resultEjercicios.error) {
              console.log(resultEjercicios.error);
            } else {
              for (const result of resultEjercicios) {

                const wodEjercicio = new WodEjercicio();

                wodEjercicio.id = result.idc_wod_ejercicios;
                wodEjercicio.nombre = result.nombre;
                wodEjercicio.repeticiones = result.repeticiones;
                wodEjercicio.peso = result.peso;


                wod.ejercicios.push(wodEjercicio);
              }
            }

          });

          this.listadoWods.push(wod);
        }
      }
    });
  }

  containsCategoria(idCategoria: number) {
    let exist = -1;
    for (let idx = 0; idx < this.ejerCategorias.length; idx++) {
      if (idCategoria == this.ejerCategorias[idx].idCategoria) {
        exist = idx;
      }
    }
    return exist;
  }

  toggleMarca(categoria: DetalleCategoria) {
    if (categoria.isShowMarca) {
      categoria.isShowMarca = false;
    } else {
      categoria.isShowMarca = true;
    }
  }

  validMarca(reps, minute, seconds) {
    let isValid = true;
    if (!reps || reps === '') {
      isValid = false;
    }
    if (!minute || minute === '') {
      isValid = false;
    }
    if (!seconds || seconds === '') {
      isValid = false;
    }

    return isValid;
  }

  showAlert(title: string, subtitle: string) {
    let alert = this.alertCrtl.create({
      title: title,
      subTitle: subtitle,
      buttons: ['OK']
    });
    alert.present();
  }

  presentaModalMarca() {
    let modal = this.modalCtrl.create(SaveRecordPage, { wods: this.listadoWods, miembro: this.idMiembro });
    modal.present();
  }

  openCalendar() {
    this.showCalendar = true;
  }

  onSelectDate(event) {
    console.log(event);
    if (event._d !== null) {
      this.todayWod = event._d;
      this.loadWodDetails();
      this.showCalendar = false;
    }
  }


}
