import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { WOD, DetalleCategoria } from '../../app/wod.entity';
import { WodsProvider } from '../../providers/wods/wods';
import { Utils } from '../../app/utils';

@Component({
  selector: 'page-ranking',
  templateUrl: 'ranking.html',
})
export class RankingPage {

  searchBy: string = 'reps';
  wod: WOD;
  emptyMessage: string;
  marcasComplete = new Array<DetalleCategoria>();
  todayWod: Date;
  categoria:number = 1;
  filtro:string = 'reps';

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private wodsProvider: WodsProvider) {
    this.wod = this.navParams.get('wod');
    this.todayWod = new Date();
  }

  ionViewDidLoad() {
    this.rankingByReps();
  }

  onselect(){
    this.emptyMessage = undefined;
    if(this.filtro === 'reps') {
      this.rankingByReps();
    } else {
      this.rankingByTime();
    }
  }

  rankingByReps() {
    this.marcasComplete = new Array<DetalleCategoria>();
    const ids = new Array();
    this.wodsProvider.getDayWods(new Date()).subscribe(resultWods => {

      if (resultWods.error) {

      } else {
        for (const resWod of resultWods) {
          ids.push(resWod.idc_wods);
        }
      }
      console.log(ids.toString());


      this.wodsProvider.getRankingOrderReps(ids.toString(),this.categoria).subscribe(marcasResult => {
        console.log(marcasResult);
        if (marcasResult.error) {
          this.emptyMessage = 'No se ha registrado ninguna marca.';
        } else {
          for (const marca of marcasResult) {
            const detalle = new DetalleCategoria();
            const wod = new WOD();

            wod.id = marca.idc_wod_ejercicios;
            wod.nombre = marca.wod_desc;

            detalle.wod = wod;

            detalle.idCategoria = marca.idc_categoria;
            detalle.descripcion = marca.cat_desc;
            detalle.repsTotal = marca.repeticiones;
            detalle.peso = marca.peso;
            detalle.timeTotal = Utils.getTimeFromSecods(marca.timecap);
            detalle.fechaRegistro = new Date(marca.fecha_registro);
            detalle.nombreMiembro = marca.nombre_completo;

            this.marcasComplete.push(detalle);
          }
        }
      });

    });
  }

  rankingByTime() {
    this.marcasComplete = new Array<DetalleCategoria>();
    const ids = new Array();
    this.wodsProvider.getDayWods(new Date()).subscribe(resultWods => {

      if (resultWods.error) {

      } else {
        for (const resWod of resultWods) {
          ids.push(resWod.idc_wods);
        }
      }
      console.log(ids.toString());

      this.wodsProvider.getRankingOrderTime(ids.toString(),this.categoria).subscribe(marcasResult => {
        console.log(marcasResult);
        if (marcasResult.error) {
          this.emptyMessage = 'No se ha registrado ninguna marca.';
        } else {
          for (const marca of marcasResult) {
            const detalle = new DetalleCategoria();
            const wod = new WOD();

            wod.id = marca.idc_wod_ejercicios;
            wod.nombre = marca.wod_desc;

            detalle.wod = wod;

            detalle.idCategoria = marca.idc_categoria;
            detalle.descripcion = marca.cat_desc;
            detalle.repsTotal = marca.repeticiones;
            detalle.peso = marca.peso;
            detalle.timeTotal = Utils.getTimeFromSecods(marca.timecap);
            detalle.fechaRegistro = new Date(marca.fecha_registro);
            detalle.nombreMiembro = marca.nombre_completo;

            this.marcasComplete.push(detalle);
          }
        }
      });

    });
  }



}
