import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { TerminosPage } from '../terminos/terminos';
import { AutenticacionProvider } from '../../providers/autenticacion/autenticacion';
import { MicuentaPage } from '../micuenta/micuenta';
import { HomePage } from '../home/home';

@Component({
  selector: 'page-registro',
  templateUrl: 'registro.html',
})
export class RegistroPage {

  nombreCompleto: string;
  email: string;
  password: string;
  confirma: string;
  aceptaTerminos = false;

  msgNombreEmpty = false;
  msgEmailEmpty = false;
  msgPassEmpty = false;
  msgPassConfirmEmpty = false;
  msgPassConfirm = false;
  msgTerminos = false;


  constructor(public navCtrl: NavController, public navParams: NavParams,
    public autenticacionProvider: AutenticacionProvider, public alertCtrl: AlertController) {

  }

  goTerminosCondiciones() {
    this.navCtrl.push(TerminosPage);
  }

  registraUsuario() {

    if (this.validaRegistro()) {
      this.autenticacionProvider.registrarUsuario(this.email, this.password)
        .then(result => {
          console.log(result);
          this.autenticacionProvider.guardaCuentaAuth(this.nombreCompleto, this.email).subscribe(result => {
            console.log(result);
            let alert = this.alertCtrl.create({
              title: "Grandioso!!",
              message: "Tu cuenta se registro correctamente.",
              buttons: ['OK']
            });
            alert.present();
            this.navCtrl.setRoot(HomePage);
          }, err => {
            console.log(err);
            let alert = this.alertCtrl.create({
              title: "Error al registrar",
              message: "Error => " + err,
              buttons: ['OK']
            });
            alert.present();
          });
        })
        .catch(error => {
          let alert = this.alertCtrl.create({
            title: "Error al registrar",
            message: "Error => " + error,
            buttons: ['OK']
          });
          alert.present();
        });
    }
  }

  cancelaRegistro() {
    this.navCtrl.pop();
  }

  validaRegistro() {
    console.log(this.nombreCompleto + "," + this.email + "," + this.password + "," + this.confirma);
    let okForm = true;
    if (this.isEmpty(this.nombreCompleto)) {
      this.msgNombreEmpty = true;
      okForm = false;
    } else {
      this.msgNombreEmpty = false;
    }
    if (this.isEmpty(this.email)) {
      this.msgEmailEmpty = true;
      okForm = false;
    } else {
      this.msgEmailEmpty = false;
    }
    if (this.isEmpty(this.password)) {
      this.msgPassEmpty = true;
      okForm = false;
    } else {
      this.msgPassEmpty = false;
    }


    if (!this.aceptaTerminos) {
      this.msgTerminos = true;
      okForm = false;
    } else {
      this.msgTerminos = false;
    }

    if (okForm) {
      return true;
    } else {
      return false;
    }
  }

  isEmpty(value) {
    return (value == null || value === '');
  }

}
