import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ToastController } from 'ionic-angular';
import { WodsProvider } from '../../providers/wods/wods';
import { DetalleCategoria, WOD } from '../../app/wod.entity';
import { Utils } from '../../app/utils';

@Component({
  selector: 'page-mismarcas',
  templateUrl: 'mismarcas.html',
})
export class MismarcasPage {

  idMiembro: number = 0;
  marcasComplete: Array<DetalleCategoria>;
  emptyMessage: string;
  searchBy: string = 'reps';

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private wodsProvider: WodsProvider, private alertCtrl: AlertController,
    private toastCtrl: ToastController) {
    this.idMiembro = this.navParams.get('idMiembro');
    this.marcasComplete = new Array<DetalleCategoria>();
    this.searchByDate();
  }

  ionViewDidLoad() {

  }

  changeSearch(by: string) {
    if (by === 'reps') {
      this.marcasComplete = new Array<DetalleCategoria>();
      this.searchByReps();
    } else {
      this.marcasComplete = new Array<DetalleCategoria>();
      this.searchByTime();
    }
  }

  searchByDate() {
    this.marcasComplete = new Array<DetalleCategoria>();
    this.wodsProvider.getAllResultadoOrderByDate(this.idMiembro).subscribe(marcasResult => {
      console.log(marcasResult);
      if (marcasResult.error) {
        this.emptyMessage = 'No tienes marcas registradas';
      } else {
        for (const marca of marcasResult) {
          const detalle = new DetalleCategoria();
          const wod = new WOD();

          wod.id = marca.idc_wod_ejercicios;
          wod.nombre = marca.wod_desc;
          wod.fechaRegistroCadena = Utils.formatDate(marca.wod_fecha);

          detalle.wod = wod;

          detalle.id = marca.idt_resultados_wod_miembro;
          detalle.idCategoria = marca.idc_categoria;
          detalle.descripcion = marca.cat_desc;
          detalle.repsTotal = marca.repeticiones;
          detalle.peso = marca.peso;
          detalle.timeTotal = Utils.getTimeFromSecods(marca.timecap);
          detalle.fechaRegistro = marca.fecha_registro;
          detalle.fechaRegistroCadena = Utils.formatDate(marca.fecha_registro);

          this.marcasComplete.push(detalle);
        }
      }
    });
  }

  searchByReps() {
    this.marcasComplete = new Array<DetalleCategoria>();
    this.wodsProvider.getAllResultadoOrderReps(this.idMiembro).subscribe(marcasResult => {
      console.log(marcasResult);
      if (marcasResult.error) {
        this.emptyMessage = 'No tienes marcas registradas';
      } else {
        for (const marca of marcasResult) {
          const detalle = new DetalleCategoria();
          const wod = new WOD();

          wod.id = marca.idc_wod_ejercicios;
          wod.nombre = marca.wod_desc;

          detalle.wod = wod;

          detalle.idCategoria = marca.idc_categoria;
          detalle.descripcion = marca.cat_desc;
          detalle.repsTotal = marca.repeticiones;
          detalle.timeTotal = Utils.getTimeFromSecods(marca.timecap);
          detalle.fechaRegistro = new Date(marca.fecha_registro);

          this.marcasComplete.push(detalle);
        }
      }
    });
  }

  searchByTime() {
    this.marcasComplete = new Array<DetalleCategoria>();
    this.wodsProvider.getAllResultadoOrderTime(this.idMiembro).subscribe(marcasResult => {
      console.log(marcasResult);
      if (marcasResult.error) {
        this.emptyMessage = 'No tienes marcas registradas';
      } else {
        for (const marca of marcasResult) {
          const detalle = new DetalleCategoria();
          const wod = new WOD();

          wod.id = marca.idc_wod_ejercicios;
          wod.nombre = marca.wod_desc;

          detalle.wod = wod;

          detalle.idCategoria = marca.idc_categoria;
          detalle.descripcion = marca.cat_desc;
          detalle.repsTotal = marca.repeticiones;

          detalle.timeTotal = Utils.getTimeFromSecods(marca.timecap);
          detalle.fechaRegistro = marca.fecha_registro;

          this.marcasComplete.push(detalle);
        }
      }
    });
  }

  swipeEvent(marca: DetalleCategoria) {
    this.showConfirm(marca);
  }

  showConfirm(marca: DetalleCategoria) {
    console.log(marca.id);
    marca.toDelete = true;
    let confirm = this.alertCtrl.create({
      title: 'Eliminar marca',
      message: 'Esta accion definitiva, ¿Esta seguro que desea eliminar esta marca?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('Disagree clicked');
            marca.toDelete = false;
          }
        },
        {
          text: 'Eliminar',
          handler: () => {
            console.log('Agree clicked');
            this.deleteRecord(marca.id);
          }
        }
      ]
    });
    confirm.present();
  }

  deleteRecord(idRecord: number) {
    this.wodsProvider.deleteRecord(idRecord).subscribe(resultDelete => {
      if (resultDelete.error) {
        this.presentToast(resultDelete.error);
      } else {
        this.presentToast(resultDelete.success);
        this.searchByDate();
      }
    });
  }

  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 2000,
      position: 'middle'
    });
    toast.present();
  }
}
