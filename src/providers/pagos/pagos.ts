import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Config } from '../../app/config';

@Injectable()
export class PagosProvider {

  config = new Config();

  constructor(public http: Http) {
    console.log('Hello PagosProvider Provider');
  }


  getPagosActivosByMiembroOrdeByNewest(idMiembro: number) {
    console.log(idMiembro);
    return this.http.post(this.config.getHost() + '/atomicabe/dispositivo/pagosActivosByMiembro.php', {
      id_miembro: idMiembro
    })
      .map(res => res.json());
  }
}
