import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Config } from '../../app/config';

@Injectable()
export class WodsProvider {

  config = new Config();

  constructor(public http: Http) {
    
  }

  getDayWods(wodDate:Date) {
    const todayanio = wodDate.getFullYear();
    const todaymes = wodDate.getMonth() + 1;
    const todaydia = wodDate.getDate();
    //const todaydia = 23;
    console.log(todayanio + ", " + todaymes + ", " + todaydia);
    
    return this.http.post(this.config.host + '/atomicabe/catalogos/getWodsByDay.php', {
      pass_key: this.config.passkey,
      dia: todaydia,
      mes: todaymes,
      anio: todayanio
    })
      .map(res => res.json());
  }
  
  getEjerciciosByWod(idWod: number) {
    return this.http.post(this.config.getHost() + '/atomicabe/catalogos/wods.ejercicios.php', {
      pass_key: this.config.getPassKey(),
      wod_id: idWod
    })
      .map(res => res.json());
  }

  getCatEjercicioBywodExej(idWodExers: string) {
    return this.http.post(this.config.getHost() + '/atomicabe/dispositivo/wods.ejercicios.categoria.php', {
      pass_key: this.config.getPassKey(),
      wod_ejercicio_ids: idWodExers
    })
      .map(res => res.json());
  }


  saveResultadoWODCategoria(idWod: number,idMiembro: number,idCategoria: number,repeticiones: number, timecap:number, peso:string) {
    return this.http.post(this.config.getHost() + '/atomicabe/dispositivo/saveMarcaWOD.php', {
      pass_key: this.config.getPassKey(),
      id_wod: idWod,
      id_miembro: idMiembro,
      id_categoria: idCategoria,
      reps: repeticiones,
      time_cap: timecap,
      peso:peso
    })
      .map(res => res.json());
  }

  getAllResultadoOrderReps(idMiembro: number) {
    return this.http.post(this.config.getHost() + '/atomicabe/dispositivo/getAllMyRecordsByReps.php', {
      pass_key: this.config.getPassKey(),
      id_miembro: idMiembro
    })
      .map(res => res.json());
  }

  getAllResultadoOrderByDate(idMiembro: number) {
    return this.http.post(this.config.getHost() + '/atomicabe/dispositivo/getAllMyRecordsByDate.php', {
      pass_key: this.config.getPassKey(),
      id_miembro: idMiembro
    })
      .map(res => res.json());
  }

  getAllResultadoOrderTime(idMiembro: number) {
    return this.http.post(this.config.getHost() + '/atomicabe/dispositivo/getAllMyRecordsByTime.php', {
      pass_key: this.config.getPassKey(),
      id_miembro: idMiembro
    })
      .map(res => res.json());
  }

  getBestResultadoOrderReps(idMiembro: number, idsWods:string) {
    return this.http.post(this.config.getHost() + '/atomicabe/dispositivo/getBestRecordsByReps.php', {
      pass_key: this.config.getPassKey(),
      id_miembro: idMiembro,
      ids_wod: idsWods
    })
      .map(res => res.json());
  }

  getBestResultadoOrderTime(idMiembro: number,idWod:number) {
    return this.http.post(this.config.getHost() + '/atomicabe/dispositivo/getBestRecordsByTime.php', {
      pass_key: this.config.getPassKey(),
      id_miembro: idMiembro,
      id_wod: idWod
    })
      .map(res => res.json());
  }

  getRankingOrderReps(ids:string,idCategoria:number) {
    return this.http.post(this.config.getHost() + '/atomicabe/dispositivo/getRankingByReps.php', {
      pass_key: this.config.getPassKey(),
      ids_wods: ids,
      id_categoria:idCategoria
    })
      .map(res => res.json());
  }

  getRankingOrderTime(ids:string,idCategoria:number) {
    return this.http.post(this.config.getHost() + '/atomicabe/dispositivo/getRankingByTime.php', {
      pass_key: this.config.getPassKey(),
      ids_wods: ids,
      id_categoria:idCategoria
    })
      .map(res => res.json());
  }
  
  deleteRecord(id:number) {
    return this.http.post(this.config.getHost() + '/atomicabe/dispositivo/deleteRecord.php', {
      pass_key: this.config.getPassKey(),
      id_marca: id
    })
      .map(res => res.json());
  }
}
