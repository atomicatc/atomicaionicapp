import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Config } from '../../app/config';


@Injectable()
export class AutenticacuentaProvider {

    config = new Config();

    constructor(public http: Http) {
        console.log('Hello AutenticacuentaProvider Provider');
    }

    getMiembroByUUID(uuid: string) {
        return this.http.post(this.config.getHost() + '/atomicabe/dispositivo/getMiembrosByUUID.php', {
            pass_key: this.config.getPassKey(),
            uuidstring: uuid
        }).map(res => res.json());
    }

    activaCuenta(id: number) {
        return this.http.post(this.config.getHost() + '/atomicabe/dispositivo/activa.cuenta.php', {
            pass_key: this.config.getPassKey(),
            id_miembro: id
        }).map(res => res.json());
    }
}
