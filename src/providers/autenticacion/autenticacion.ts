import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import firebase from 'firebase';
import { Config } from '../../app/config';

@Injectable()
export class AutenticacionProvider {

  config = new Config();

  constructor(public http: Http) {
    console.log('Hello AutenticacionProvider Provider');
  }

  registrarUsuario(correo: string, clave: string) {
    return firebase.auth().createUserWithEmailAndPassword(correo, clave);
  }

  iniciarSesion(correo: string, clave: string) {
    return firebase.auth().signInWithEmailAndPassword(correo, clave);
  }

  validaSesion() {
    return firebase.auth();
  }

  terminaSesion() {
    return firebase.auth().signOut();
  }

  guardaCuentaAuth(nombre: string, correo: string) {
    return this.http.post(this.config.getHost() + '/atomicabe/catalogos/miembros.save.auth.php', {
      pass_key: this.config.getPassKey(),
      nombre_completo: nombre,
      correo_electronico: correo
    }).map(res => res.json());
  }

  getMiembroByEmail(correo: string){
     return this.http.post(this.config.getHost() + '/atomicabe/dispositivo/getMiembrosByEmail.php', {
      pass_key: this.config.getPassKey(),
      correo_electronico: correo
    }).map(res => res.json());
  }

  updateDateAccount(idMiembro:number, nombre:string, objetivoUser:string, consideracionesUser:string, telefono:string){
    return this.http.post(this.config.getHost() + '/atomicabe/dispositivo/miembros.update.php', {
      pass_key: this.config.getPassKey(),
      id_miembro:idMiembro,
      nombre_completo:nombre,
      objetivo:objetivoUser,
      consideraciones:consideracionesUser,
      telefono_principal:telefono
    })
      .map(res => res.json());
  }

}
