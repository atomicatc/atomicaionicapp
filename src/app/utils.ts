
import * as moment from 'moment';

export class Utils {

    static getTimeFromSecods(secs) {

        var sec_num = parseInt(secs, 10)
        var hours = Math.floor(sec_num / 3600) % 24
        var minutes = Math.floor(sec_num / 60) % 60
        var seconds = sec_num % 60
        return [hours, minutes, seconds]
            .map(v => v < 10 ? "0" + v : v)
            .filter((v, i) => v !== "00" || i > 0)
            .join(":")

    }

    static getSecodsFromTime(time) {
        var parts = time.split(':'),
            minutes = +parts[0],
            seconds = +parts[1];
        return (minutes * 60 + seconds);
    }


    static calculateNextDate(startDate: Date, next: number, metric: string) {
        if (metric === 'D') {
            return new Date(startDate.setTime(startDate.getTime() + next * 86400000));
        } else {
            return new Date(startDate.setMonth(startDate.getMonth() + next * 1));
        }
    }
    

    static isABBRange(startRange: Date, endRage: Date, date2Test: Date) {
        if (date2Test.getTime() < startRange.getTime()) {
            return 'after';
        } else if (date2Test.getTime() > endRage.getTime()) {
            return 'before';
        } else {
            return 'between';
        }
    }

    static formatDate(date2Format:Date){
        return moment(date2Format).format('DD/MMM/YYYY')
    }

}
