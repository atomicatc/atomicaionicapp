export class Pago{
    id:number;
    concepto: string;
    importe:number;
    fechaRegistro: Date;
    fechaRegistroCadena: string;
}