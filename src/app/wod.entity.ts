export class WOD {
    id: number;
    nombre: string;
    tipo: number;
    tipoDesc: string;
    series: string;
    timecap: string
    participacion: string;
    detalle: string;
    ejercicios: Array<any>;
    fechaRegistro:Date;
    fechaRegistroCadena:string;

    static fromJson({ idc_wod_ejercicio, nombre, series, timecap, tipo, wod_tipo }) {
        const wod = new WOD();

        wod.id = idc_wod_ejercicio;
        wod.nombre = nombre;
        wod.series = series;
        wod.timecap = timecap;
        wod.tipo = tipo;
        wod.tipoDesc = wod_tipo;

        return wod;
    }
}

export class WodEjercicio {
    id: number;
    nombre: string;
    repeticiones: number;
    categorias: Array<EjercicioCategoria>;
    peso: string;
}

export class EjercicioCategoria {
    id: number;
    categoriaId: number;
    categoriaDesc: string;
    nombre: string;
    reps: number;
    base: string;
}

export class DetalleCategoria {
    id:number;
    wod: WOD;
    nombreMiembro: string;
    idCategoria: number;
    descripcion: string;
    ejercicio: Array<EjercicioCategoria>;
    isShowMarca: Boolean = false;
    repsTotal: number = 0;
    minuteTotal: number;
    secondTotal: number;
    timeTotal: string;
    fechaRegistro: Date;
    fechaRegistroCadena: String;
    imgCategoria: string;
    peso:string;
    toDelete:Boolean = false;
}
