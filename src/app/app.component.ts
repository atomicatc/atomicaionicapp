import { Component, ViewChild } from '@angular/core';
import { Platform, NavController, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import firebase from 'firebase';

import { HomePage } from '../pages/home/home';
import { LandingPage } from '../pages/landing/landing';
import { AcercadePage } from '../pages/acercade/acercade';
import { MicuentaPage } from '../pages/micuenta/micuenta';
import { Storage } from '@ionic/storage';
import { AutenticacionProvider } from '../providers/autenticacion/autenticacion';
import { MismarcasPage } from '../pages/mismarcas/mismarcas';

import { Events } from 'ionic-angular';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild('contenido') menu: NavController;

  /**
   * Catalogo de paginas disponibles para el menu.
   */
  rootPage: any = LandingPage;
  principal = LandingPage;
  micuenta = MicuentaPage;
  acercade = AcercadePage;
  mismarcas = MismarcasPage;

  /**
   * Ubicacion de la imagen del perfil.
   */
  fullPathImage: string = './assets/img/profile.png';
  userName: String = '';
  idMiembro: number = 0;

  showMenu: Boolean = false;
  KEY_PATH_IMG_PROFILE = "imgProfile";

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,
    private storage: Storage, private menuCtrl: MenuController, private autenticacionProvider: AutenticacionProvider,
    public events: Events) {

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });

    firebase.initializeApp({
      apiKey: "AIzaSyBeGvIT0KHhbF6W8ZbBT-D0uZf818eq-To",
      authDomain: "atomicatrainingcenter.firebaseapp.com"
    });

    /**
     * Evento de log in de sesion de usuario.
     */
    events.subscribe('user:login', (user, logged, pathImg, idMiembro) => {
      this.showMenu = logged;
      this.userName = user;
      this.idMiembro = idMiembro;
    });

    /**
     * Evenro de actualizacion y ajustes de la imagen de perfil.
     */
    events.subscribe('user:updatepic', (pathImg) => {
      // user and time are the same arguments passed in `events.publish(user, time)`
      this.fullPathImage = pathImg;
    });
  }

  /**
   * Metodo para ir a la pagina selecionada del menu.
   * @param page Pagina donde se va a ir.
   */
  goPage(page: any) {
    this.menu.setRoot(page, { idMiembro: this.idMiembro });
    this.menuCtrl.close();
  }

  /**
   * Metodo para cerrar la cession y limpiar los parametros registrados durante el uso.
   */
  terminaSesion() {
    this.autenticacionProvider.terminaSesion()
      .then(info => {
        this.storage.remove('isLoggedIn');
        this.storage.remove('userlogged');
        this.storage.remove('passwordlogin');

        this.menu.setRoot(HomePage);
        this.menuCtrl.close().then(closed => {
          this.events.publish('user:login', '', false, '');
        });
      });
  }
}

