import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule} from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { CalendarModule } from "ion2-calendar";

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { AutenticacuentaProvider } from '../providers/autenticacuenta/autenticacuenta';
import { Device } from '@ionic-native/device';
import { NovalidaPage } from '../pages/novalida/novalida';
import { MicuentaPage } from '../pages/micuenta/micuenta';
import { PagosProvider } from '../providers/pagos/pagos';

import { File } from '@ionic-native/file';
import { Transfer } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';

import { IonicStorageModule } from '@ionic/storage';
import { AcercadePage } from '../pages/acercade/acercade';
import { RegistroPage } from '../pages/registro/registro';
import { TerminosPage } from '../pages/terminos/terminos';
import { AutenticacionProvider } from '../providers/autenticacion/autenticacion';
import { LandingPage } from '../pages/landing/landing';
import { DetailWodPage } from '../pages/detail-wod/detail-wod';
import { MismarcasPage } from '../pages/mismarcas/mismarcas';
import { WodsProvider } from '../providers/wods/wods';
import { RankingPage } from '../pages/ranking/ranking';
import { SaveRecordPage } from '../pages/save-record/save-record';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    NovalidaPage,
    MicuentaPage,
    AcercadePage,
    RegistroPage,
    TerminosPage,
    LandingPage,
    DetailWodPage,
    MismarcasPage,
    RankingPage,
    SaveRecordPage
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot({
      name: '__mydb',
         driverOrder: ['indexeddb', 'sqlite', 'websql']
    }),
    CalendarModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    NovalidaPage,
    MicuentaPage,
    AcercadePage,
    RegistroPage,
    TerminosPage,
    LandingPage,
    DetailWodPage,
    MismarcasPage,
    RankingPage,
    SaveRecordPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AutenticacuentaProvider,
    Device,
    PagosProvider,
    File,
    Transfer,
    Camera,
    FilePath,
    AutenticacionProvider,
    AutenticacionProvider,
    BarcodeScanner,
    WodsProvider
  ]
})
export class AppModule {}
