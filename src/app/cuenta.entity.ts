export class Cuenta {
    idMembresia: number;
    email: string;
    estado: number;
    fechaIngreso: Date;
    fechaRegistro: Date;
    fechaUltimoPago: Date;
    fechaSiguientePago: Date;
    fechaIngresoCadena: string;
    fechaRegistroCadena: string;
    fechaUltimoPagoCadena: string;
    fechaSiguientePagoCadena: string;
    id: number;
    isActivo: number;
    nombreCompleto: string;
    telefonoPrincipal: string;
    uuid: string;
    objetivo:string;
    consideraciones:string;
}